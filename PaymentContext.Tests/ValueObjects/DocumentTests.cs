﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaymentContext.Domain.Enums;
using PaymentContext.Domain.ValueObjects;

namespace PaymentContext.Tests.ValueObjects
{
    [TestClass]
    public class DocumentTests
    {
        //  Metodologia => Red, Green, Refactor
        //  1ª : Fazer os testes falhare
        //  2ª : Fazer os testes passarem
        //  3ª : Refatorar o codigo

        //  Red
        [TestMethod]
        public void RetornarUmErroQuandoCNPJForInvalido()     // public void ShouldReturnErrorWhenCNPJIsInvalid()
        {
            var doc = new Document("123", EDocumentType.CNPJ);
            Assert.IsTrue(doc.Invalid);   //  Garanta que é verdadeiro, que o meu documento é inválido.
        }

        //  Green
        [TestMethod]
        public void RetornarSucessoQuandoCNPJForValido()     // public void ShouldReturnSuccessWhenCNPJIsValid()
        {
            var doc = new Document("34110468000150", EDocumentType.CNPJ);
            Assert.IsTrue(doc.Valid);   //  Garanta que é verdadeiro, que o meu documento é válido.
        }

        [TestMethod]
        public void RetornarUmErroQuandoCPFForInvalido()     // public void ShouldReturnErrorWhenCPFIsInvalid()
        {
            var doc = new Document("123", EDocumentType.CPF);
            Assert.IsTrue(doc.Invalid);   //  Garanta que é verdadeiro, que o meu documento é inválido.
        }

        [TestMethod]
        [DataTestMethod]
        [DataRow("34225545806")]
        [DataRow("54139739347")]
        [DataRow("01077284608")]
        public void RetornarSucessoQuandoPCFForValido(string cpf)     // public void ShouldReturnSuccessWhenCPFIsValid()
        {
            var doc = new Document(cpf, EDocumentType.CPF);
            Assert.IsTrue(doc.Valid);   //  Garanta que é verdadeiro, que o meu documento é válido.
        }
    }
}
