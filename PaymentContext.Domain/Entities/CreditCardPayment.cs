using PaymentContext.Domain.ValueObjects;
using System;

namespace PaymentContext.Domain.Entities
 {
    public class CreditCardPayment : Payment
    {
        public CreditCardPayment(
            string cardHoldenName,
            string cardNumber,
            string lastTransactionNumber,
            DateTime paidDate,
            DateTime expireDate,
            decimal total,
            decimal totalPaid,
            string payer,
            Document document,
            Address address,
            Email email) : base(
                                paidDate,
                                expireDate,
                                total,
                                totalPaid,
                                payer,
                                document,
                                address,
                                email)
        {
            CardHoldenName = cardHoldenName;
            CardNumber = cardNumber;
            LastTransactionNumber = lastTransactionNumber;
        }

        public string CardHoldenName { get; private set; }
        public string CardNumber { get; private set; }
        public string LastTransactionNumber { get; private set; }
    }

 }