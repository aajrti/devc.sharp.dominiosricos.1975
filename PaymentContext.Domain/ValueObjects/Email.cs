﻿// VO São objetos de valor que compõem uma entidade.
//  VO é persistido sempre dentro de uma entidade
//  e persistido no BD.

using Flunt.Validations;
using PaymentContext.Shared.ValueObjects;

namespace PaymentContext.Domain.ValueObjects
{
    public class Email : ValueObject
    {
        public Email(string address)
        {
            Address = address;

            AddNotifications(new Contract()
                .Requires()
                .IsEmail(Address, "Email.Address", "E-mail inválido")
                );
        }

        public string Address { get; private set; }
    }
}
