﻿// VO São objetos de valor que compõem uma entidade.
//  VO é persistido sempre dentro de uma entidade
//  e persistido no BD.

using Flunt.Validations;
using PaymentContext.Shared.ValueObjects;

namespace PaymentContext.Domain.ValueObjects
{
    public class Name : ValueObject
    {
        public Name(string firtName, string lastName)
        {
            FirtName = firtName;
            LastName = lastName;

            AddNotifications(new Contract()
                .Requires()
                .HasMinLen(FirtName, 3, "Name.FirstName", "Nome deve conter pelo menos 3 caracteres")
                .HasMinLen(LastName, 3, "Name.LastName", "Nome deve conter pelo menos 3 caracteres")
                );
        }

        public string FirtName { get; private set; }
        public string LastName { get; private set; }
    }
}
