﻿// VO São objetos de valor que compõem uma entidade.
//  VO é persistido sempre dentro de uma entidade
//  e persistido no BD.

using Flunt.Validations;

using PaymentContext.Domain.Enums;
using PaymentContext.Shared.ValueObjects;

namespace PaymentContext.Domain.ValueObjects
{
    public class Document : ValueObject
    {
        public Document(string number, EDocumentType type)
        {
            Number = number;
            Type = type;

            AddNotifications(new Contract()
                .Requires()
                .IsTrue(ValidateDocument(), "Document.Number", "Documento inválido")
                );
        }

        public string Number { get; private set; }
        public EDocumentType Type { get; private set; }

        private bool ValidateDocument()
        {
            if (Type == EDocumentType.CNPJ)
                return IsCnpj(Number);

            if (Type == EDocumentType.CPF)
                return IsCpf(Number);

            return false;
        }

        /// <summary>
        /// Realiza a validação do CNPJ
        /// </summary>
        /// <param name="cnpj"></param>
        /// <returns></returns>
        private static bool IsCnpj(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;

            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");

            if (cnpj.Length != 14)
                return false;

            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;

            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];

            resto = (soma % 11);

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();
            //tempCnpj = tempCnpj + digito;
            tempCnpj += digito;
            soma = 0;

            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];

            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            //digito = digito + resto.ToString();
            digito += resto.ToString();
            return cnpj.EndsWith(digito);
        }

        /// <summary>
        /// Realiza a validação do CPF
        /// </summary>
        /// <param name="cpf"></param>
        /// <returns></returns>
        private static bool IsCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }


        //internal static bool Cpf(int cpf)
        //{
        //    string strCpf = cpf.ToString().PadLeft(11, '0');

        //    if (strCpf.All(x => x == strCpf[0]))
        //        return false;

        //    var listCpf = strCpf.Select(num => Convert.ToInt32(num.ToString())).ToList();

        //    if (listCpf[9] != Mod11Cpf(listCpf, 10))
        //        return false;

        //    if (listCpf[10] != Mod11Cpf(listCpf, 11))
        //        return false;

        //    return true;
        //}

        //internal static int Mod11Cpf(List<int> elementos, int @base)
        //{
        //    int soma = 0;
        //    for (int i = 0; i < (@base - 1); i++)
        //        soma += (@base - i) * elementos[i];

        //    int dv1, resto = soma % 11;

        //    if (resto < 2)
        //        dv1 = 0;
        //    else
        //        dv1 = 11 - resto;

        //    return dv1;
        //}
    }
}
