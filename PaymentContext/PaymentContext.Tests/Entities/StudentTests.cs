using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaymentContext.Domain.Entities;

namespace PaymentContext.Tests
{
    [TestClass]
    public class StudentTests
    {
        [TestMethod]
        public void AdicionaAssinatura()
        {
            var subscription = new Subscription(null);
            var student = new Student("Alcides", "Assunção", "102030", "aajr.ti@gmail.com");
            student.AddSubscription(subscription);
        }
    }
}
