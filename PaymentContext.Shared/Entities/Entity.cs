﻿using Flunt.Notifications;
using System;

namespace PaymentContext.Shared.Entities
{
    public abstract class Entity : Notifiable
    {
        protected Entity()
        {
            Id = Guid.NewGuid();    //  É menos performatico, mas é controlado no .NET
        }

        public Guid Id { get; private set; }
    }
}
